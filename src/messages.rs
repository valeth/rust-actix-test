use bytes::Bytes;
use tokio::net::TcpStream;
use actix::Message;

use crate::connection::ConnectionId;


#[derive(Message)]
pub struct Connect(pub TcpStream);

#[derive(Message)]
pub struct Disconnect(pub ConnectionId);

#[derive(Message)]
pub struct Raw(pub Bytes);
