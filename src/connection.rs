use std::time::{Duration, Instant};

use log::{debug, warn, info};
use uuid::Uuid;
use actix::prelude::*;
use crate::{
    messages::{Raw, Disconnect},
    gateway::Gateway,
};


#[derive(Debug, Hash, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ConnectionId(Uuid);

impl ConnectionId {
    pub fn new() -> Self {
        Self(Uuid::new_v4())
    }
}

impl std::fmt::Display for ConnectionId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}


#[derive(Debug)]
pub struct Connection {
    id: ConnectionId,
    gateway: Addr<Gateway>,
    last_heartbeat: Instant,
}

impl Connection {
    pub fn new(id: ConnectionId, gateway: Addr<Gateway>) -> Self {
        Self { id, gateway, last_heartbeat: Instant::now() }
    }
}

impl Actor for Connection {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        info!("RTMP connection {} opened", self.id);

        ctx.run_interval(Duration::from_secs(5), |act, ctx| {
            if Instant::now().duration_since(act.last_heartbeat) > Duration::from_secs(10) {
                warn!("Heartbeat failed, stopping client {}", act.id);
                ctx.stop();
            }
        });
    }

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        self.gateway.do_send(Disconnect(self.id.clone()));
        Running::Stop
    }

    fn stopped(&mut self, _ctx: &mut Self::Context) {
        info!("RTMP connection {} closed", self.id);
    }
}

impl Handler<Raw> for Connection {
    type Result = ();

    fn handle(&mut self, msg: Raw, _ctx: &mut Self::Context) -> Self::Result {
        debug!("Got {} bytes", msg.0.len());

        if !msg.0.is_empty() {
            self.last_heartbeat = Instant::now();
        }
    }
}
