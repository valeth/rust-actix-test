use std::{
    collections::HashMap,
    net::{SocketAddr, ToSocketAddrs},
};

use log::{debug, error, info};
use actix::prelude::*;
use tokio::{
    net::TcpListener,
    io::AsyncRead,
    codec::{FramedRead, BytesCodec},
};

use crate::{
    messages::{Connect, Disconnect, Raw},
    connection::{Connection, ConnectionId},
};


#[derive(Debug)]
pub struct Gateway {
    addr: SocketAddr,
    peers: HashMap<ConnectionId, Addr<Connection>>,
}

impl Gateway {
    pub fn new() -> Self {
        Self::with_addr("0.0.0.0:1945").expect("Parser bug")
    }

    pub fn with_addr<A>(addr: A) -> Result<Self, String>
        where A: ToSocketAddrs
    {
        let addr = addr.to_socket_addrs()
            .map(|mut a| a.next())
            .map_err(|e| e.to_string())?
            .ok_or_else(|| "Failed to parse address".to_string())?;
        Ok(Self { addr, peers: HashMap::new() })
    }
}

impl Actor for Gateway {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        let listener = TcpListener::bind(&self.addr)
            .expect("Failed to bind to TCP address")
            .incoming()
            .map_err(|e| error!("{}", e))
            .map(Connect);
        ctx.add_message_stream(listener);

        info!("Listening on {}", self.addr);
    }
}

impl Handler<Connect> for Gateway {
    type Result = ();

    fn handle(&mut self, msg: Connect, ctx: &mut Self::Context) -> Self::Result {
        let gateway_addr = ctx.address();
        let conn_id = ConnectionId::new();
        let conn_id2 = conn_id.clone();
        let conn_addr = Connection::create(move |ctx| {
            let (rx, _tx) = msg.0.split();
            let reader = FramedRead::new(rx, BytesCodec::new())
                .map_err(|e| error!("{}", e))
                .map(|x| Raw(x.into()));
            ctx.add_message_stream(reader);
            Connection::new(conn_id2, gateway_addr)
        });
        self.peers.insert(conn_id, conn_addr);
    }
}

impl Handler<Disconnect> for Gateway {
    type Result = ();

    fn handle(&mut self, msg: Disconnect, _ctx: &mut Self::Context) -> Self::Result {
        let conn_id = msg.0;
        if let Some(conn_addr) = self.peers.remove(&conn_id) {
            debug!("Removed {:?} from connection registry", conn_addr);
            debug!("Remaining entries in registry: {}", self.peers.len());
        }
    }
}
