#![warn(rust_2018_idioms)]
#![warn(clippy::all)]


mod messages;
mod connection;
mod gateway;


use simplelog::{
    TermLogger,
    LevelFilter as LogLevel,
    Config as LoggerConfig,
};
use actix::prelude::*;

use self::gateway::Gateway;


fn main() -> std::io::Result<()> {
    if let Err(e) = TermLogger::init(LogLevel::Debug, LoggerConfig::default()) {
        eprintln!("Failed to initialize logger {}", e);
    }

    let system = actix::System::new("main");

    Gateway::new().start();

    system.run()
}
